Vue.component('pselect', {
  props: ['url', 'value'],
  template: '<div class="pselect" style="position:relative;">' +
  '<pmasked mask="ems" @click="popupShow()" class="form-control lk-cat-add-form-item__input" style="width:100%" @input="inputChange" />' +
  '<div class="popup">' +
  '<a @click="valSet($event, item)"  style="display:block;" href="#" v-for="item in suggestions">{{ item.title }}{{ item.color_text ? \' (\' + item.color_text + \')\' : \'\' }}</a></div>' +
  '</div>',
  data: function () {
    return {
      suggestions: []
    }
  },
  created: function () {
    this.inputChange = _.debounce(this.inputChange, 300)
  },
  mounted: function () {
    var self = this;
    $('body').click(function (e) {
      if (!$(e.target).closest('.pselect').length) {
        self.popupHide()
      }
    })
  },
  methods: {
    popupShow: function () {
      $(this.$el).find('.popup').show()
    },
    popupHide: function () {
      $(this.$el).find('.popup').hide()
    },
    inputChange: function (e) {

      var self = this;
      var value = $.trim(e);

      axios.get(self.url + '?term=' + value).then(function (result) {
        self.suggestions = _.assign([], self.suggestions, result.data.results);
        _.each(self.suggestions, function (item) {
          if (item.title == value) {
            self.$emit('input', item.id);
            return false
          }
        })
      });

      self.popupShow()
    },
    valSet: function (e, val) {
      var self = this;
      e.preventDefault();
      self.$emit('input', val.id);
      self.popupHide()
    }
  },
  watch: {
    value: function (value) {
      var self = this;
      if (value) {
        axios.get(reverse('api:ColorViewSet-detail', {pk: value})).then(function (result) {
          $(self.$el).find('input').val(result.data['title']);
          self.$emit('input', result.data['id'])
          bus.$emit('psuggest_change', self);
        }).catch(function (error) {
          console.log(error.data)
        });
      } else {
        $(self.$el).find('input').val('');
      }
    }
  }
});