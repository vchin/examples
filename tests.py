# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import base64
from time import sleep

from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.test import TestCase
from django.core.files import File
from django.core.cache.backends import locmem
from django.core import mail
from django.conf import settings
from django.test import override_settings
from django.utils import timezone

from rest_framework.test import APIClient

from datetime import datetime, timedelta, time

from . import models as auction_models
from ..core import models as core_models
from faker import Factory


class PetTest(TestCase):
    def setUp(self):
        from django.core.management import call_command
        call_command('clear_index', interactive=0)

        fake = Factory.create()

        self.client = APIClient()

        self.client.force_authenticate(core_models.User.objects.get(pk=4))
        self.user = self.client.handler._force_user
        self.pet = self.user.pet_set.all()[0]
        with open(os.path.join(settings.BASE_DIR, 'assets', '3.jpg'),
                'rb') as f:
            self.pet_image = core_models.PetImage.objects.create(
                user=self.pet.user,
                name='pet image test',
                pet=self.pet,
                created='2016-02-02')
            self.pet_image.image.save('img_1.jpg',
                File(f),
                save=True)

    @override_settings(DEBUG=True)
    def test_pet_image(self):
        url = reverse('api:pet-image-set-list-create-update')
        with open(os.path.join(settings.BASE_DIR, 'assets', '1.png'),
                'rb') as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode(
                'utf-8').replace('\n', '')
            img = self.pet_image.image.url
            result = self.client.post(url,
                {'items': [
                    {'id': self.pet_image.pk,
                     'pet': self.pet.pk,
                     'created_at': '2016-02-02',
                     'image': '',
                     # 'image': 'data:image/png;base64,{}'.format(
                     # encoded_string),
                     'name': 'test_pet'},
                    {'id': 2,
                     'pet': self.pet.pk,
                     'created_at': '2016-02-02',
                     'image': 'data:image/png;base64,{}'.format(
                         encoded_string),
                     'name': 'test_pet_2'}
                ]},
                format='json'
            )

            self.assertEqual(
                self.pet.petimage_set.filter(name='test_pet').first().image.url,
                img)
            self.assertTrue(
                self.pet.petimage_set.filter(name='test_pet').exists())

            self.assertFalse(
                self.pet.petimage_set.filter(name='pet image test').exists()
            )
