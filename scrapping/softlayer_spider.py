import os
import codecs
import scrapy
from pyquery import PyQuery as PQ
import ujson
from scrapy.utils.project import get_project_settings
from unipath import Path
from apps.common.utils import file_get_contents

CURRENT_PATH = Path(__file__).ancestor(1)


class SoftlayerSpider(scrapy.Spider):
    name = 'softlayer'
    allowed_domains = ['softlayer.com']
    start_urls = [
        'http://www.softlayer.com/bare-metal-search',
        # 'http://ya.ru'
    ]

    is_debug = True

    def parse(self, response):
        st = get_project_settings()
        pq = PQ(response.body)

        script_text = 'window.SCRAPY=true;'

        if self.is_debug:
            script_text += 'window.SCRAPY_DEBUG=true;'

        # load promise lib
        file_path = os.path.join(CURRENT_PATH, 'js', 'lib', 'q.js')
        script_text += file_get_contents(file_path)

        # load spider lib
        file_path = os.path.join(CURRENT_PATH, 'js',
                                 '{}.js'.format(self.name))
        script_text += file_get_contents(file_path)

        # with codecs.open(
        #         os.path.join(),
        #                      encoding='utf-8') as f:
        #     script_text = f.read()
        #

        # assert(splash:autoload("https://code.jquery.com/jquery-2.1.3.min.js"))

        # splash:autoload([[
        #         function get_document_title(){
        #            return document.title;
        #         }
        #     ]])
        # lua = '''
        # function main(splash)
        #     local url = splash.args.url
        #     assert(splash:go(url))
        #     assert(splash:wait(3.5))
        #     splash:runjs([[$('input[data-categorycode="datacenter"]').eq(1).click()]])
        #     return {html=splash:html()}
        #   end
        # '''
        # f = codecs.open('unicode.rst', encoding='utf-8')


        lua = '''
            function main(splash)
                local url = splash.args.url
                assert(splash:autoload([[{script_text}]]))
                assert(splash:go(url))
                assert(splash:wait(3.5))

                local result, error = splash:wait_for_resume([[
                    function main(splash) {{
                        parse(splash);
                    }}
                ]])

                return {{result=result, error=error}}
            end
        '''.format(script_text=script_text)

        lua2 = '''
            function main(splash)
                local url = splash.args.url
                assert(splash:autoload([[{script_text}]]))
                assert(splash:go(url))
                assert(splash:wait(3.5))

                local result, error = splash:wait_for_resume([[
                    function main(splash) {{
                        setTimeout(function(){{
                            splash.resume({{ok:'111'}});
                        }}, 30000)
                    }}
                ]])

                return {{result=result, error=error}}

            end
        '''.format(script_text=script_text)

        # lua = '''
        # function main(splash)
        #     local url = splash.args.url
        #     assert(splash:autoload({}))
        #     assert(splash:go(url))
        #     assert(splash:wait(3.5))
        #
        #     splash:runjs([[ {} ]])'''.format(script_text)
        #
        # lua += '''local result = splash:evaljs('parse();')
        #     return {data=result}
        #   end
        # '''


        timeout = 200 # in sec, for json api answer
        for i, item in enumerate(pq('.sl-store-product')):
            href = 'https://www.softlayer.com{}'.format(
                PQ(item)('a').attr('href'))
            # href = 'http://vestlite.ru'

            yield scrapy.Request(href, self.parse_link, meta={
                'splash': {
                    'args': {
                        'lua_source': lua,
                        'timeout' : timeout
                     },
                    'endpoint': 'execute',
                    'console': 1
                },
                'download_timeout': timeout # 3 min

            })

            if i == 1:
                break
                # print time.time() - start_time

    def parse_link(self, response):
        # pq = PQ(response.body)
        res = ujson.loads(response.body)
        pass

        # print(res['html'])
        # print(res['tempo'])
