###
#   DRF examples
#
###
class AuthManagerViewSet(viewsets.GenericViewSet):
    # filter_backends = [PetViewSetBackend]
    email_template_name = 'apps/activation/mails/password_reset.j2',

    permission_classes = [permissions.AllowAny]

    def get_serializer_class(self):
        user = self.request.user
        if self.action == 'reset_password':
            class _Serializer(serializers.Serializer):
                username = serializers.CharField()

                def validate(self, attrs):
                    try:
                        self.user = custom_models.User.objects.get(
                            email=attrs['username'])
                    except Exception as e:
                        raise serializers.ValidationError(
                            {'username': [_('invalid email')]})

                    return attrs

            return _Serializer
        elif self.action == 'change_password':
            class _Serializer(serializers.Serializer):
                password_0 = serializers.CharField()
                password_1 = serializers.CharField()
                password_2 = serializers.CharField()

                def validate(self, attrs):
                    if not user.check_password(attrs['password_0']):
                        raise serializers.ValidationError(
                            {'password_0': [_('invalid old password')]})
                    if attrs['password_1'] != attrs['password_2']:
                        raise serializers.ValidationError({
                            'password_1': [_('Passwords not equal')],
                            'password_2': [_('Passwords not equal')]
                        })

                    return attrs

            return _Serializer
        elif self.action == 'auth':
            class _Serializer(serializers.Serializer):
                username = serializers.EmailField()
                password = serializers.CharField()

                def validate(self, attrs):
                    err = {
                        'username': [_('wrong password or email')],
                        'password': [_('wrong password or email')]
                    }

                    user = custom_models.User.objects.filter(
                        email=attrs['username']).first()

                    if not user:
                        raise serializers.ValidationError(err)

                    user_auth = authenticate(
                        username=user.email,
                        password=attrs['password'],
                    )

                    if not user_auth:
                        raise serializers.ValidationError(err)

                    attrs['user_auth'] = user_auth
                    return attrs

            return _Serializer

        return None

    @list_route(['post'])
    def reset_password(self, request, *args, **kwargs):
        srz = self.get_serializer(data=request.data)
        srz.is_valid(raise_exception=True)

        # if srz.is_phone:
        #     # TODO: maybe add phone rcovery
        #     pass
        #
        site = get_current_site(request)
        token = PasswordResetTokenGenerator().make_token(srz.user)

        url = 'http://{}{}'.format(site.domain,
            reverse('activation:password_reset_confirm',
                kwargs={'uidb64': urlsafe_base64_encode(
                    force_bytes(srz.user.pk)), 'token': token}))

        html = render_to_string(self.email_template_name, {
            'user': srz.user,
            'url': url,
            'site': site
        })
        try:
            mail_send(srz.user.email, message_html=html,
                subject=_('password reset'))
            return Response({'success': True, 'email': srz.user.email})
        except Exception as e:
            return Response({'success': False, 'error': str(e)})

        return Response({'success': True})

    @list_route(['post'])
    def change_password(self, request, *args, **kwargs):
        srz = self.get_serializer(data=request.data)
        srz.is_valid(raise_exception=True)
        request.user.set_password(srz.validated_data['password_1'])
        request.user.save()

        login(request, request.user,
            backend='django.contrib.auth.backends.ModelBackend')

        return Response({'success': True})

    @list_route(['post'])
    def auth(self, request, *args, **kwargs):
        srz = self.get_serializer(data=request.data)
        srz.is_valid(raise_exception=True)

        login(request, srz.validated_data['user_auth'])

        return Response({'success': True})


class PetImageViewSet(viewsets.ModelViewSet):
    """
    Image
    """
    permission_classes = [permissions.IsAuthenticated]
    pagination_class = _paginate_cls(30, 30)

    # filter_backends = [PetViewSetBackend]

    def filter_queryset(self, queryset):
        if self.action == 'list':
            return super(PetImageViewSet, self).filter_queryset(queryset)
        return queryset

    @list_route(['get', 'post'])
    def list_create_update(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        instances = common_utils.qset_to_dict(queryset)
        ids = {}
        for item in request.data['items']:
            if 'id' in item:
                ids[item['id']] = item['id']

        if len(ids) or not len(request.data['items']):
            queryset.exclude(id__in=ids.keys()).delete()

        for item in request.data['items']:
            instance = instances.get(item.get('id', None), None)
            if instance and not item['image'] or item['image'].startswith(
                    'http'):
                item['image'] = instance.image if instance.image else None

            serializer = self.get_serializer(instance=instance,
                data=item)
            serializer.is_valid(raise_exception=True)
            serializer.save()

        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        return core_models.PetImage.objects.filter(
            user=self.request.user, pet_id=self.kwargs['pet_id'])

    def get_serializer_class(self):
        class _Serializer(serializers.ModelSerializer):
            image = Base64ImageField()
            parser_classes = [parsers.FormParser, parsers.MultiPartParser]

            class Meta:
                model = core_models.PetImage
                fields = ['id', 'pet', 'image', 'name', 'created', 'is_cover']

        return _Serializer
