 CREATE OR REPLACE FUNCTION public.history_hstore (
  )
  RETURNS trigger AS
  $body$
    DECLARE
      tbl_name TEXT := TG_ARGV[0];
      tbl_fields TEXT := TG_ARGV[1];
      tbl_values TEXT := TG_ARGV[2];
      tbl_fcheck TEXT[] := TG_ARGV[3];
      old_values hstore;
      new_values hstore;
      old_changes hstore;
      new_changes hstore;
    BEGIN
      IF (TG_OP = 'UPDATE') THEN
        old_values := slice(hstore(OLD), tbl_fcheck);
        new_values := slice(hstore(NEW), tbl_fcheck);
        old_changes := old_values - new_values;
        new_changes := new_values - old_values;

        IF old_changes != ''::hstore OR new_changes != ''::hstore THEN
          EXECUTE format(
            'INSERT INTO %I (%s, old_values, new_values) '
            'VALUES (%s, $3, $4)',
              TG_TABLE_NAME || tbl_name,
              tbl_fields, tbl_values
          ) USING OLD, NEW, old_changes, new_changes;
        END IF;

        RETURN NEW;
      END IF;
    EXCEPTION
    WHEN unique_violation THEN
    --  statements;
    END;
  $body$
  LANGUAGE 'plpgsql'
  VOLATILE
  CALLED ON NULL INPUT
  SECURITY DEFINER
  COST 100;