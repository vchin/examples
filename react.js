import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {Layout, MenuTop, MenuAccount, Logo} from 'components';
import {connect} from 'react-redux';
import Modal from '../modal/Modal';
import {modalActions, accountActions, basketActions, paymentActions} from 'store/actions'
import 'ui/common.less'
import 'ui/icons.css'
import './App.less'

class App extends Component {

    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount() {
        this.props.dispatch(accountActions.auth());
        let query = {...this.props.location.query};
        if (query.m) {
            //delete query.m;
            //this.context.router.push({
            //    pathname: this.props.location.pathname,
            //    query: query
            //});
            this.props.dispatch(modalActions.open(this.props.location.query.m));
        }
        if (query.reset_password_url){
            this.props.dispatch(modalActions.open('reset-password',{reset_password_url: query.reset_password_url}));
        }
    }

    componentWillReceiveProps(nextProps) {
        // Обновить корзину после входа/выхода пользователя
        if (nextProps.account.profile !== this.props.account.profile) {
            this.props.dispatch(basketActions.load());
        }
        // Сброс оплаты, если корзина обновилась
        if (nextProps.basket.order !== this.props.basket.order) {
            this.props.dispatch(paymentActions.reset());
        }
    }


    goRegistration = (e) => {
        e.preventDefault();
        this.props.dispatch(modalActions.open('registration', {}));
    };

    goLogin = (e) => {
        e.preventDefault();
        this.props.dispatch(modalActions.open('login', {}));
    };

    goLoginTender = (e) => {
        e.preventDefault();
        this.props.dispatch(modalActions.open('login', {
            close: (result) => {
                if (result === 'success'){
                    this.context.router.push('/account/tenders/add');
                }
            }
        }));
    };

    goLogout = (e) => {
        e.preventDefault();
        this.props.dispatch(accountActions.logout());
    };

    getAccountItems() {
        const {account, basket} = this.props;
        const basketCount = (basket.order && basket.order.product_set.length) ? `(${basket.order.product_set.length})` : '';
        let items = [
            //{'key':'basket', 'title':`Корзина${basketCount}`, 'to':'/basket'}
        ];
        if (account.profile.user) {
            items = [
                {'key': 'account', 'title': account.profile.nickname, 'to': '/account'},
                {'key': 'logout', 'title': 'Выход', 'to': '#', onClick: this.goLogout}
            ].concat(items);
            if (!account.profile.copyright_agreement){
                items.unshift(
                    {'key': 'reg-author', 'title': 'Стать автором', 'to': '/reg-author'},
                    //{'key': 'tender-add', 'title': 'Добавить тендер', 'to': '/account/tenders/add'},
                );
            }
        } else {
            items = [
                {'key': 'reg-author', 'title': 'Стать автором', 'to': '/reg-author'},
                {'key': 'registration', 'title': 'Регистрация', 'to': '#', onClick: this.goRegistration},
                {'key': 'login', 'title': 'Вход', 'to': '#', onClick: this.goLogin}
            ].concat(items)
        }
        return items;
    }

    getTopItems() {
        const {account} = this.props;
        var items = [
            {key: 'start-earning', title: 'Как стать автором', to: '/start-earning'},
            {key: 'how-to-buy', title: 'Как купить', to: '/how-to-buy'},
            {key: 'about', title: 'О нас', to: '/about'},
            //{key: 'shares', title: 'Акции', to: '/shares'},
            {key: 'tenders', title: 'Тендеры', to: '/tenders'}
        ];
        if (account.profile.user && !account.profile.copyright_agreement) {
            items.push(
                {'key': 'tender-add', 'title': 'Заказать музыку', className: 'btn btn_normal btn_spec', 'to': '/account/tenders/add'},
            );
        }else
        if (!account.profile.user){
            items.push(
                {'key': 'tender-add', 'title': 'Заказать музыку', className: 'btn btn_normal btn_spec', 'to': '#', onClick: this.goLoginTender},
            );
        }
        items.push(
            {key: 'help', title: '?', to: '/faq', classNameModifier: 'circle'}
        );

        return items;
    }

    getFooterItems() {
        var items = [
            {key: 'about', title: 'О нас', to: '/about'},
            {key: 'user-agreement', title: 'Пользовательское соглашение', to: 'https://tunestock.ru/terms_of_use.pdf', target:'_blank'},
            {key: 'contacts', title: 'Контакты', to: '/contacts'},
            {key: 'help', title: 'Помощь/FAQ', to: '/faq'},
            {key: 'legal-issues', title: "Правовые вопросы", to: '/legal-issues'}
        ];
        return items;
    }

    render() {
        const {basket:{order}} = this.props;
        const basketCount = (order && order.product_set.length) ? order.product_set.length : '';

        return (
            <div className="App">
                <Layout
                    header={[
                        <div key="logo" className="App__logo"><Logo link="/" key="logo_header"/></div>,
                        <div key="basket" className="MenuBasket">
                            <Link to='/basket' title="Корзина покупок"><i className="fa fa-shopping-cart"/> {basketCount}</Link>
                        </div>,
                        <MenuAccount items={this.getAccountItems()} key="menu-account_header"/>,
                        <MenuTop items={this.getTopItems()} key="menu-top_header"/>
                    ]}
                    center={this.props.children}
                    footer={<div>
                        <div className="App__footer-head">
                            <Logo link="/" key="logo_footer"/>
                        </div>
                        <div className="App__border-line"></div>
                        <div>
                            <MenuTop items={this.getFooterItems()} key="menu-top-footer"/>
                            <div className="socials">
                            Мы в соцсетях:
                                <a href="https://www.facebook.com/tunestock.ru/" target="_blank" className="facebook">
                                <i className="fa fa-facebook"/>
                              </a>

                              <a href="http://vk.com/tunestock" target="_blank" className="vk">
                                <i className="fa fa-vk"/>
                              </a>

                              <a href="https://twitter.com/tunestock" target="_blank" className="twitter">
                                <i className="fa fa-twitter"/>
                              </a>
                            </div>
                             <div className="App__copyrights">
                            Все права защищены.
                            </div>
                        </div>
                    </div>}
                />
                <Modal/>
            </div>
        );
    }
}

export default connect(state => {
    return {
        views: state.views,
        account: state.account,
        basket: state.basket
    }
})(App);