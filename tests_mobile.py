# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import unittest, time, re


class TestsRxshops(unittest.TestCase):
    def setUp(self):
        # self.driver = webdriver.PhantomJS(**TEST_LOCAL_SETTINGS)
        # self.driver = webdriver.PhantomJS('./phantomjs')
        self.driver = webdriver.PhantomJS()
        self.driver.implicitly_wait(30)
        self.urls = [
            'http://mobile.shop1.rxshop.dev/',
            'http://mobile.shop3.rxshop.dev/',
            'http://mobile.shop4.rxshop.dev/',
            'http://mobile.shop5.rxshop.dev/'
        ]
        self.shop1_url = 'http://mobile.shop1.rxshop.dev/'
        self.shop3_url = 'http://mobile.shop3.rxshop.dev/'
        self.shop4_url = 'http://mobile.shop4.rxshop.dev/'
        self.shop5_url = 'http://mobile.shop5.rxshop.dev/'
        self.target_product = 'viagra'
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_all_shops(self):
        driver = self.driver
        for url in self.urls:
            driver.get(url + '/')
            # Testing menu
            menu_elements_len = len(driver.find_elements_by_css_selector('.m_menu > ul > li > a'))
            for i in range(1, menu_elements_len+1):
                driver.find_element_by_css_selector('.m_menu > div.button > span').click()
                driver.find_element_by_xpath('(//*[@class="m_menu"]/ul/li/a)[' + str(i) + ']').click()
                driver.back()

            # Testing error messages in form of contacts when all elements of form is empty
            driver.find_element_by_css_selector('.m_menu > div.button > span').click()
            driver.find_element_by_xpath('//*[@class="m_menu"]/ul/li/a[@href="/contact/"]').click()
            driver.find_element_by_css_selector('.m_submit > .m_button').click()
            fiedsets_len = len(driver.find_elements_by_xpath('//*[@class="m_grid_lr"]/li/fieldset'))
            errors_len = len(driver.find_elements_by_xpath('//*[@class="m_grid_lr"]/li/fieldset/*[@class="errors "]'))
            assert fiedsets_len == errors_len

            # Testing switcher of currency
            driver.find_element_by_xpath('//*[@data-rx_currency_selected="currency_selected"]').click()
            currency = driver.find_element_by_xpath('(//*[@data-rx_currency="currency"]/*)[2]')
            currency_text = str(currency.text)
            currency.click()
            assert currency_text == driver.find_element_by_xpath('//*[@data-rx_currency_selected="currency_selected"]').text.encode('utf8')

            # Testing switcher of languages
            language_original = driver.find_element_by_xpath('//*[@data-rx_lng_selected="lng_selected"]')
            language_original_text = language_original.text.lower()
            language_original.click()
            driver.find_element_by_xpath('(//*[@data-rx_lng="lng"]/*)[2]').click()
            driver.find_element_by_xpath('//*[@data-rx_lng_selected="lng_selected"]').click()
            target_language_text = unicode(driver.find_element_by_xpath('(//*[@data-rx_lng="lng"]/*)[2]').text).lower()
            language_new_text = unicode(driver.find_element_by_xpath('//*[@data-rx_lng_selected="lng_selected"]').text).lower()
            assert target_language_text == language_new_text != language_original_text
            driver.find_element_by_xpath('(//*[@data-rx_lng="lng"]/*)[1]').click()
            assert language_original_text == unicode(driver.find_element_by_xpath('//*[@data-rx_lng_selected="lng_selected"]').text).lower() != target_language_text

            # Testing product on index page
            driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()
            product_header_link = driver.find_element_by_xpath('(//*[@class="m_product_list_element"]/*[@class="product-header"]/a)')
            product_name = product_header_link.text.lower()
            product_header_link.click()
            assert product_name == driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="product-title"]/*[@class="title"]').get_attribute('innerHTML').lower()
            driver.back()
            driver.find_element_by_xpath('//*[@class="m_product_list_element"]/a').click()
            assert product_name == driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="product-title"]/*[@class="title"]').get_attribute('innerHTML').lower()
            driver.back()
            driver.find_element_by_xpath('//*[@class="m_product_list_element"]/*[@class="product-button"]/*[@class="button"]').click()
            assert product_name == driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="product-title"]/*[@class="title"]').get_attribute('innerHTML').lower()

            # Testing package buy
            target_product_qty = driver.find_element_by_xpath('(//*[@data-rx="package-qty"])[1]').text.lower()
            target_product_qty_upgrade = driver.find_element_by_xpath('(//*[@data-rx="package-qty"])[2]').text.lower()
            target_product_name = driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="product-title"]/*[@class="title"]').get_attribute('innerHTML').lower()
            driver.find_element_by_xpath('//*[@data-rx="buy"]').click()
            current_product_text = driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .package').text.lower()
            assert target_product_qty in current_product_text
            assert target_product_name in current_product_text
            driver.back()

            # Testing "other names" on product page
            target_product = driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="other-names"]/a')
            target_product_text = target_product.text.lower()
            target_product.click()
            assert target_product_text == driver.find_element_by_xpath('//*[@class="m_product_info"]/*[@class="product-title"]/*[@class="title"]').get_attribute('innerHTML').lower()
            driver.find_element_by_xpath('//*[@data-rx="buy"]').click()
            driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()

            # Testing cart package-list link
            driver.find_element_by_xpath('//*[@class="m_cart"]/*[@class="cart"]').click()
            driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .package').click()
            driver.back()

            # Testing cart package-list remove button
            assert int(len(driver.find_elements_by_xpath('//*[@class="m_package_list"]/*[@class="package-list"]/*[@class="package-list-element"]'))) == 2
            driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .remove').click()
            assert int(len(driver.find_elements_by_xpath('//*[@class="m_package_list"]/*[@class="package-list"]/*[@class="package-list-element"]'))) == 1

            # Testing cart package-list upgrade
            assert target_product_qty_upgrade in driver.find_element_by_xpath('//*[@class="m_package_list"]/*[@class="package-list"]/*[@class="upgrade"]/a').text.lower()
            assert target_product_qty_upgrade not in driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .package').text.lower()
            driver.find_element_by_xpath('//*[@class="m_package_list"]/*[@class="package-list"]/*[@class="upgrade"]/a').click()
            assert target_product_qty_upgrade not in driver.find_element_by_xpath('//*[@class="m_package_list"]/*[@class="package-list"]/*[@class="upgrade"]/a').text.lower()
            assert target_product_qty_upgrade in driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .package').text.lower()

            # Testing buttons 'continue shopping' and 'checkout'
            driver.find_element_by_css_selector('.m_button_continue_shopping > a').click()
            driver.find_element_by_xpath('//*[@class="m_cart"]/*[@class="cart"]').click()
            driver.find_element_by_css_selector('.m_price_button_checkout > .button').click()

    def test_shop1(self):
        driver = self.driver
        driver.get(self.shop1_url + '/')

        # Test bottom menu list
        menu_element = driver.find_element_by_xpath('//*[@class="m_menu_list"]/*[@class="menu-list"]/a')
        menu_element.click()
        driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()

        # Testing search form
        driver.find_element_by_name('search').clear()
        driver.find_element_by_name('search').send_keys(self.target_product)
        driver.find_element_by_css_selector('input[type="submit"]').click()
        assert self.target_product.lower() == driver.find_element_by_css_selector('.m_product_list_element > .product-header > a').text.lower()
        driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()

        # Testing category list on index page
        target_category = driver.find_element_by_xpath('//*[@class="m_category_list"]/a')
        target_category_name = target_category.text.lower()
        target_category.click()
        assert target_category_name == driver.find_element_by_xpath('//*[@class="m_product_accordion_list"]/*[@class="product-accordion-title selected"]').text.lower()

        # Testing cart package-list price == subtotal
        driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()
        driver.find_element_by_xpath('//*[@class="m_product_list_element"]/a').click()
        driver.find_element_by_xpath('//*[@data-rx="buy"]').click()
        price = unicode(driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .price').text).lower()
        subtotal = unicode(driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .subtotal').text).lower()
        assert price == subtotal

        # Testing accordione on page bestsellers
        driver.find_element_by_css_selector('.m_menu > div.button').click()
        driver.find_element_by_xpath('//*[@class="m_menu"]/ul/li/a[@href="/categories/bestsellers/"]').click()
        assert 'selected' not in driver.find_element_by_xpath('//*[@class="m_product_accordion_list"]/*[@class="product-accordion-title"]').get_attribute('class')
        driver.find_element_by_css_selector('.m_product_accordion_list > .product-accordion-title').click()
        assert 'selected' in driver.find_element_by_xpath('//*[@class="m_product_accordion_list"]/*[@class="product-accordion-title selected"]').get_attribute('class')
        driver.find_element_by_css_selector('.m_category_list > .m_category').click()
        assert 'selected' in driver.find_element_by_xpath('//*[@class="m_product_accordion_list"]/*[@class="product-accordion-title selected"]').get_attribute('class')
        driver.find_element_by_css_selector('.m_product_list_element > .product-button > .button').click()
        driver.back()
        driver.find_element_by_css_selector('.m_product_accordion_list > .product-accordion-title.selected').click()
        assert 'selected' not in driver.find_element_by_xpath('(//*[@class="m_product_accordion_list"]/*[@class="product-accordion-title"])[2]').get_attribute('class')

    def test_shop3(self):
        driver = self.driver
        driver.get(self.shop3_url + '/')

        # Testing category list
        driver.find_element_by_css_selector('.m_category_list > .title').click()
        category = driver.find_element_by_xpath('//*[@class="m_category_list"]/a')
        category_name = category.text.lower()
        category.click()
        assert category_name == driver.find_element_by_xpath('//*[@class="m_product_list"]/*[@class="product-title"]').text.lower()

        # Testing search form
        driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()
        driver.find_element_by_name('search').clear()
        driver.find_element_by_name('search').send_keys(self.target_product)
        driver.find_element_by_css_selector('input[type="submit"]').click()
        assert self.target_product.lower() == driver.find_element_by_css_selector('.m_product_list_element > .product-header > a').text.lower()

        # Testing cart package-list price == subtotal
        driver.find_element_by_xpath('//*[@class="m_logo"]/a').click()
        driver.find_element_by_xpath('//*[@class="m_product_list_element"]/a').click()
        driver.find_element_by_xpath('//*[@data-rx="buy"]').click()
        price = unicode(driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .price').text).lower()
        subtotal = unicode(driver.find_element_by_css_selector('.m_package_list > .package-list > .package-list-element > .subtotal').text).lower()
        assert price == subtotal


    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == '__main__':
    unittest.main()
