###
# Поддержка динамических сессий rx_session, для доменов которые часто блокируют
#
#
###
import time
import logging
import random
import urlparse
from datetime import datetime
from django.utils import timezone
from collections import OrderedDict
from django.shortcuts import redirect
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
from django.forms import model_to_dict
from django.http import Http404, HttpResponseNotFound, HttpResponse
from django.utils.cache import patch_vary_headers
from django.utils.http import cookie_date
from django.utils import translation
from constance import config
from rxcore.core.thread_locals import set_thread_var
from rxcore.core.utils import qset_to_dict, get_client_ip, cart_get, ShopSettings
from rxcore.orders.models import Cart, CartItem, Shipping, SessionLink, Order
from rxcore.products.models import ProductOverride, ProductPack
from rxcore.shops.models import Shop, Theme
from rxcore.users.models import User
from rxshop.shops.views import geo_ip_info_get, rx_format_currency
from rxshop.shops.statistics import hits_dump
from . import session

logger = logging.getLogger('middleware')

import re

reg_b = re.compile(
    r"(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino",
    re.I | re.M)
reg_v = re.compile(
    r"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\\-(n|u)|c55\\/|capi|ccwa|cdm\\-|cell|chtm|cldc|cmd\\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\\-s|devi|dica|dmob|do(c|p)o|ds(12|\\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\\-|_)|g1 u|g560|gene|gf\\-5|g\\-mo|go(\\.w|od)|gr(ad|un)|haie|hcit|hd\\-(m|p|t)|hei\\-|hi(pt|ta)|hp( i|ip)|hs\\-c|ht(c(\\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\\-(20|go|ma)|i230|iac( |\\-|\\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\\/)|klon|kpt |kwc\\-|kyo(c|k)|le(no|xi)|lg( g|\\/(k|l|u)|50|54|\\-[a-w])|libw|lynx|m1\\-w|m3ga|m50\\/|ma(te|ui|xo)|mc(01|21|ca)|m\\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\\-2|po(ck|rt|se)|prox|psio|pt\\-g|qa\\-a|qc(07|12|21|32|60|\\-[2-7]|i\\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\\-|oo|p\\-)|sdk\\/|se(c(\\-|0|1)|47|mc|nd|ri)|sgh\\-|shar|sie(\\-|m)|sk\\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\\-|v\\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\\-|tdg\\-|tel(i|m)|tim\\-|t\\-mo|to(pl|sh)|ts(70|m\\-|m3|m5)|tx\\-9|up(\\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\\-|your|zeto|zte\\-",
    re.I | re.M)


def decorator_skip_static(fun):
    def decorator(self, request):
        if request.path.startswith('/media') and request.path.startswith('/static'):
            return

        return fun(self, request)

    return decorator


class MiddlewareDetectMobileBrowser(object):
    # @decorator_skip_static
    def process_request(self, request):
        request.is_mobile = False

        if request.META.has_key('HTTP_USER_AGENT'):
            user_agent = request.META['HTTP_USER_AGENT']

            b = reg_b.search(user_agent)
            v = reg_v.search(user_agent[0:4])

            request.is_mobile = b or v

        request.is_mobile = request.is_mobile or settings.IS_MOBILE or request.META.get('HTTP_HOST', '').startswith(
            'mobile') or request.META.get('HTTP_HOST', '').startswith('l.mobile')


class MiddlewareCurrency(object):
    # @decorator_skip_static
    def process_request(self, request):
        rx_cur_dc = OrderedDict()
        for cur in Shop.CURRENCY.choices:
            # get from custom shop settings
            if request.rx_shop:
                if getattr(request.rx_shop, 'currency_%s' % cur[0].lower(), False):
                    rx_cur_dc[cur[0]] = cur[1]
            else:
                rx_cur_dc[cur[0]] = cur[1]

        request.rx_cur_dc = rx_cur_dc

        if settings.CURRENCY_SESSION_KEY not in request.rx_session:
            if request.rx_shop:
                cur = None

                if request.rx_shop.currency_autodetect:
                    info = geo_ip_info_get(get_client_ip(request))
                    cur = settings.RXCURRENCY.get(info['country_code'], request.rx_shop.currency_default)

                if not cur in request.rx_cur_dc:
                    cur = request.rx_shop.currency_default
            else:
                cur = settings.CURRENCY['default_currency']

            request.rx_session[settings.CURRENCY_SESSION_KEY] = cur

        session_cur = request.rx_session[settings.CURRENCY_SESSION_KEY]
        request.rx_cur = session_cur if session_cur in request.rx_cur_dc else request.rx_cur_dc.keys()[0]


class MiddlewareSession(SessionMiddleware):
    def __init__(self):
        self.SessionStore = session.session_store_get()

    # @decorator_skip_static
    def process_request(self, request):
        session_key = request.COOKIES.get(settings.RX_SESSION_COOKIE_NAME, None)
        request.rx_session = self.SessionStore(session_key=session_key)

        if not request.rx_session.session_key:
            # call create and set session flag to modified
            request.rx_session.save()

        if settings.SESSION_LINK_ID in request.REQUESTS:
            session_link = SessionLink.link_get(request.REQUESTS[settings.SESSION_LINK_ID])

            if session_link:
                request.rx_session = self.SessionStore(session_key=session_link.session_id)

                if session_link.order and session_link.order.current_code:
                    request.rx_session[settings.CURRENCY_SESSION_KEY] = session_link.order.current_code

                # need for cookie change
                request.rx_session.save()
                request.rx_session.modified = True

            return redirect(request.path)

    def process_response(self, request, response):
        try:
            accessed = request.rx_session.accessed
            modified = request.rx_session.modified
        except AttributeError:
            pass
        else:
            if accessed:
                patch_vary_headers(response, ('Cookie',))
            if modified or settings.SESSION_SAVE_EVERY_REQUEST:
                if request.rx_session.get_expire_at_browser_close():
                    max_age = None
                    expires = None
                else:
                    max_age = request.rx_session.get_expiry_age()
                    expires_time = time.time() + max_age
                    expires = cookie_date(expires_time)
                # Save the session data and refresh the client cookie.
                # Skip session save for 500 responses, refs #3881.
                if response.status_code != 500:
                    request.rx_session.save()

                    response.set_cookie(settings.RX_SESSION_COOKIE_NAME,
                                        request.rx_session.session_key, max_age=max_age,
                                        expires=expires, domain=settings.RX_SESSION_COOKIE_DOMAIN,
                                        path=settings.RX_SESSION_COOKIE_PATH,
                                        secure=settings.SESSION_COOKIE_SECURE or None,
                                        httponly=settings.SESSION_COOKIE_HTTPONLY or None)
        return response


class MiddlewareLocale(object):
    # @decorator_skip_static
    def process_request(self, request):
        if request.rx_shop:
            rx_lng_dc = OrderedDict()

            for lng in Shop.LANG.choices:
                if getattr(request.rx_shop, 'lang_%s' % lng[0], False):
                    rx_lng_dc[lng[0]] = lng[1]

            request.rx_lng_default = request.rx_shop.lang_default
            request.rx_lng_dc = rx_lng_dc

        else:
            request.rx_lng_default = settings.LANGUAGE_CODE
            request.rx_lng_dc = OrderedDict(settings.LANGUAGES)

        sr = re.search('^/([A-Za-z]{2})/', request.path)

        if sr and (sr.group(1) in request.rx_lng_dc or (sr.group(1) == 'ru' and 'backend/' in request.path)):
            request.rx_lng = sr.group(1)
        else:
            request.rx_lng = request.rx_lng_default

        translation.activate(request.rx_lng)


# for minimize queries to db - set request vars
class MiddlewareRxRequestVars(object):
    def process_request(self, request):
        if request.rx_shop:
            try:
                request.rx_disabled_item = request.rx_shop.disableditem
            except Exception, e:
                request.rx_disabled_item = None


class MiddlewareCartShop(object):
    def process_exception(self, request, exception):
        if isinstance(exception, Http404):
            return HttpResponseNotFound(unicode(exception))

    def process_response(self, request, response):
        if hasattr(request, 'theme'):
            response.set_cookie('theme_id', request.rx_theme['id'])

        return response

    def _cart_create(self, request, shop):
        # new cart = new session
        # save previous settings
        dc_settings = {}

        for key in ['CURRENCY_SESSION_KEY',
                    'SUB_ACCOUNT_ID_NAME',
                    'AFFILIATE_ID_NAME']:

            key_id = getattr(settings, key)

            if key_id in request.rx_session:
                dc_settings[key_id] = request.rx_session[key_id]

        SessionStore = session.session_store_get()

        request.rx_session = SessionStore()
        request.rx_session.create()

        # shop.affiliate -> cart.user for stats!!!
        cart = Cart.objects.create(session_id=request.rx_session.session_key,
                                   user=shop.affiliate,
                                   state=Cart.STATE_TYPE.FILLING,
                                   shop_id=shop.pk,
                                   shipping=Shipping.objects.all()[0])
        # restore session vars
        if len(dc_settings):
            for key, val in dc_settings.iteritems():
                request.rx_session[key] = val

        return cart

    def __old___cart_init(self, request):
        ids = {}
        items = []
        for item in CartItem.objects.select_related('product_pack',
                                                    'product_pack__product').filter(cart=request.rx_cart).order_by(
            '-created_at'):
            ids.setdefault(item.product_pack.product.pk, [])
            # various packages, same product
            ids[item.product_pack.product.pk].append(item)

            items.append(item)

        for product_override in ProductOverride.objects.filter(product_id__in=ids.keys(), shop__pk=request.rx_shop.pk):

            for item in ids[product_override.product.pk]:
                item.product_pack.product.name = product_override.name

                if product_override.is_description:
                    item.product_pack.product.description = product_override.description

                if product_override.is_description_short:
                    item.product_pack.product.description_short = product_override.description_short

                if product_override.is_safety_info:
                    item.product_pack.product.safety_info = product_override.safety_info

                if product_override.is_side_effects:
                    item.product_pack.product.side_effects = product_override.side_effects

        request.rx_cart_item_list = items

    def process_request(self, request):
        request.rx_csc_domain = request.rx_checkout_domain = False
        request.rx_shop = request.rx_cart = None

        current_domain = getattr(settings, 'CURRENT_DOMAIN', request.META.get('HTTP_HOST'))  # get for tests

        # remove port
        domain = current_domain = current_domain.split(':')[0]

        if domain.startswith('l.'):
            domain = domain.lstrip('l.')

        if domain.startswith('mobile'):
            domain = domain.lstrip('mobile.')

        if settings.DEBUG:
            domain = domain.rstrip(':%s' % request.META['SERVER_PORT']).lstrip('www.')

        shop_queryset = Shop.objects.select_related('theme', 'checkout_theme', 'user', 'parent_shop',
                                                    'parent_shop__parent_shop')
        cart_queryset = Cart.objects.select_related('shipping', 'promo_code_default', 'promo_code_reorder', 'shop',
                                                    'shop__parent_shop')

        cart = None
        shop = None  # shop config, or if we have parent_shop - real shop config, without discounts look at ManagePriceView

        # we have 4 type of domains
        # own - partner own domain and own full config with admin, identify by domain
        # public - service owner shop with custom settings by domain
        # public link - affiliate(partner) small config over public, discount type and percent
        # parked - partner domains linked to public link, identify by domain
        # ------------------
        # affiliate_id - partner_id, subaccount_id - partner promo labels

        if domain in settings.CSC_DOMAIN:
            # label for rx_router
            request.rx_csc_domain = domain

            try:
                # get cart from csc reorder
                cart = cart_queryset.get(session_id=request.rx_session.session_key,
                                         state=Cart.STATE_TYPE.FILLING)
                shop = cart.shop
            except ObjectDoesNotExist:
                logger.debug('own csc cart to be created')

            # force switch theme, because default theme get from shop
            request.rx_theme = {'name': 'csc'}
        elif domain in settings.CHECKOUT_DOMAIN:
            # label for rx_router
            request.rx_checkout_domain = domain

            try:
                cart = cart_queryset.get(session_id=request.rx_session.session_key)
                shop = cart.shop
            except ObjectDoesNotExist:
                logger.error('redirect without rx_session')
                return HttpResponse('')

        if not shop:
            try:
                shop = shop_queryset.get(domain=domain)
            except:
                raise Http404

        shop.affiliate = shop.user  # owner == affiliate

        # public shops (CSC also included)
        if shop.shop_type == shop.SHOP_TYPE.PUBLIC:
            affiliate_id = request.REQUESTS.get(settings.AFFILIATE_ID_NAME,
                                                request.rx_session.get(settings.AFFILIATE_ID_NAME))
            if affiliate_id:
                try:
                    shop.affiliate = User.objects.get(pk=affiliate_id)
                    request.rx_session[settings.AFFILIATE_ID_NAME] = affiliate_id

                    # using shop like affiliate settings - we need only discount type
                    # and percent
                    try:
                        # try to get custom discount settings by affiliate
                        # TODO: put in cache
                        proxy = Shop.objects.get(user=shop.affiliate,
                                                 parent_shop=shop,
                                                 shop_type=Shop.SHOP_TYPE.PUBLIC_LINK)

                        # use affiliate 'discount_type', 'discount_percent'
                        # shop.discount_type = proxy.discount_type
                        # shop.discount_percent = proxy.discount_percent
                        shop = ShopSettings(shop, proxy, ['discount_type', 'discount_percent'])
                    except ObjectDoesNotExist:
                        logger.debug('no settings')

                except ObjectDoesNotExist:
                    logger.debug('no affilate')


        elif shop.shop_type == Shop.SHOP_TYPE.PARKED:
            # switch to public shop setting, except 'domain'
            affiliate = shop.user
            shop = ShopSettings(shop.parent_shop, shop, ['domain', 'id', 'pk'])
            shop.affiliate = affiliate
        else:
            shop = ShopSettings(shop)

        if not cart:
            try:
                cart = cart_queryset.get(
                    session_id=request.rx_session.session_key,
                    shop_id=shop.id,
                    state=Cart.STATE_TYPE.FILLING
                )
            except Exception, e:
                cart = self._cart_create(request, shop)

        if not hasattr(request, 'rx_theme'):
            if shop.theme:
                request.rx_theme = model_to_dict(shop.theme)
            else:
                # random theme set
                # TODO: implement shop2 theme
                dc_themes = qset_to_dict(Theme.objects.filter(type=Theme.TYPE.SHOP).exclude(name='shop2'), 'name')
                theme_id = request.COOKIES.get('theme_id', 'random')

                # check theme exists. admin can turn off
                if theme_id in dc_themes:
                    request.rx_theme = model_to_dict(dc_themes[theme_id])
                else:
                    request.rx_theme = model_to_dict(random.choice(dc_themes.values()))

        sub_account_id = request.rx_session.get(settings.SUB_ACCOUNT_ID_NAME,
                                                request.REQUESTS.get(settings.SUB_ACCOUNT_ID_NAME))
        if sub_account_id:
            request.rx_sub_account_id = sub_account_id
            request.rx_session[settings.SUB_ACCOUNT_ID_NAME] = sub_account_id

        rx_referer = request.META.get('HTTP_REFERER')

        if rx_referer:
            parsed_uri = urlparse.urlparse(rx_referer)
            if parsed_uri.hostname != current_domain:
                request.rx_session['rx_referer'] = rx_referer

        request.rx_shop = shop
        request.rx_cart = cart


    def process_response(self, request, response):
        if hasattr(request, 'rx_theme'):
            response.set_cookie('theme_id', request.rx_theme['name'])

        return response


class MiddlewareUserStat(object):
    def process_view(self, request, view_func, view_args, view_kwargs):

        if not getattr(settings, 'LOCAL', False):
            if request.user.is_authenticated():
                User.objects.filter(pk=request.user.pk).update(last_seen_ts = timezone.now())

        if not request.is_ajax() and request.rx_view['view_func_name'] == 'DefaultView':
            '''custom partner router handle requests in DefaultView'''
            hits_dump.send(sender=self.__class__, request=request, cart=request.rx_cart)


# todo: need combine with MiddlewareCartShop and/or MiddlewareCurrency
class MiddlewareCartPromo(object):
    def process_request(self, request):
        ndx = request.rx_cart.promo_index

        if request.rx_cart.promo_index >= 0 and len(config.PROMO_SETTINGS) > (ndx + 1):
            request.rx_cart.next_promo = config.PROMO_SETTINGS[ndx + 1]
            request.rx_cart.next_promo['val_locale'] = \
                rx_format_currency(request, request.rx_cart.next_promo['val'])
        else:
            request.rx_cart.next_promo = None


class MiddlewareCartShopLocale(object):
    def process_request(self, request):
        request.rx_cart_locale = cart_get(request)
